/**
 * @file device_ros.h
 * @brief ROS wrapper for the example device node
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 Sep 2020
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <ros/ros.h>
#include <ros/console.h>

#include <geometry_msgs/PoseStamped.h>


#include <cmath>
#include <iostream>
#include <chrono>

namespace acl {
namespace device {

  class DeviceROS{
    public:
      // basic constructor and destructor
      DeviceROS(const ros::NodeHandle nh, const ros::NodeHandle nhp);
      ~DeviceROS() = default;

    private:
      ros::NodeHandle nh_; // publish/subscribe node handle
      ros::NodeHandle nhp_; // private node handle

      ros::Timer timer_loc_udp_;    // timer for publishing position messages at some given rate
      ros::Subscriber sub_loc_in_;  // subcribe to messages coming from the receptor node
      ros::Publisher pub_loc_out_;  // publish message to be received by the receptor node


      // calbacks:

      /**
       * @brief      Callback to the sub_loc_in_ subscriber. 
       *             Print out the message received from the receptor node
       *
       * @param[in]  msg    incoming message
       */
      void processLocData(const geometry_msgs::PoseStamped& msg);


      /**
       * @brief      Callback to the timer_loc_udp_.
       *             Publish the message to be received by the receptor node
       *
       * @param[in]  event    timer event
       */
      void publishLocation(const ros::TimerEvent& event);

  };

} // ns device
} // ns acl
