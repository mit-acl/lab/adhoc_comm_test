/**
 * @file receptor_ros.h
 * @brief The receptor node
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 Sep 2020
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <ros/ros.h>
#include <ros/console.h>

#include <geometry_msgs/PoseStamped.h>

#include <cmath>
#include <iostream>
#include <chrono>

// adhoc comm library
#include "adhoc_comm/udp_socket.h"
#include "adhoc_comm/olsrd_wrapper.h"
#include "adhoc_comm/msg_wrapper.h"

namespace acl {
namespace receptor {

  class ReceptorROS{
    public:
      /**
       * @brief      Constructor that takes two ros node handles - one for publish/subscribe, one for parameters
       *
       * @param[in]  nh    global namespace node handle ("")
       * @param[in]  nhp    prviate namespace node handle ("~")
       */
      ReceptorROS(const ros::NodeHandle nh, const ros::NodeHandle nhp);
      /**
       * @brief      Destructor that takes two ros node handles - one for publish/subscribe, one for parameters
       */
      ~ReceptorROS() = default;

      /**
       * @brief      Holds the thread until a udp message is received. 
       *             Processes the message and publishes it on the respective topic
       */
      void listenToUDP();

    private:
      /**
        * @brief      Initialize adhoc communication variables, grab parameters
        */
      void init();
      

      /**
       * @brief      callback to the sub_loc_out_ subscriber.
       *             UDP the received message to the list of olsrd neighbors.
       *
       * @param[in]  msg    message received on the topic
       */
      void sendPoseStamped(const geometry_msgs::PoseStamped& msg);


      ros::NodeHandle nh_, nhp_;    // node handles - one for topics, one for parameters
      ros::Publisher pub_loc_in_;   // publish received location messages on this topic
      ros::Subscriber sub_loc_out_; // subscribe to this topic and UDP all messages published there

      int deviceID_;                // the ID of the device

      // adhoc communication variables
      std::unique_ptr<acl::adhoc_comm::UDPSocket> udp_socket_;
      std::unique_ptr<acl::adhoc_comm::msgWrapper> msg_wrapper_;
      std::unique_ptr<acl::adhoc_comm::OLSRD_Wrapper> olsrd_;
  };

} // ns utils
} // ns acl
