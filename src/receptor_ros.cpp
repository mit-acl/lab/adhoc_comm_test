/**
 * @file receptor_ros.cpp
 * @brief ROS wrapper for the receptor node
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 Sep 2020
 */

#include "device/receptor_ros.h"
#include <chrono>
#include <thread>


namespace acl {
namespace receptor {

// ----------------------------------------------------------------------------

ReceptorROS::ReceptorROS(const ros::NodeHandle nh, const ros::NodeHandle nhp)
: nh_(nh), nhp_(nhp)
{
  init();
  pub_loc_in_ = nhp_.advertise<geometry_msgs::PoseStamped>("loc_in", 1);
  sub_loc_out_ = nh_.subscribe("device/loc_out", 1, &ReceptorROS::sendPoseStamped, this);
  
  ROS_INFO_STREAM("RECEPTOR:\tInit complete\n");
}

// ----------------------------------------------------------------------------

void ReceptorROS::listenToUDP() {  
  {
    //create a buffer for the message we will receive
    uint8_t buffer[acl::adhoc_comm::UDPSocket::PACKET_LENGTH];
    
    // wait for the message; store it in the buffer
    udp_socket_->receive( (char *)buffer, acl::adhoc_comm::UDPSocket::PACKET_LENGTH);

    // get packet ID from the buffer
    uint8_t packetID = msg_wrapper_->getPacketID(buffer, acl::adhoc_comm::UDPSocket::PACKET_LENGTH);

    // iterate through possible message types, act accordingly

    if (packetID == acl::adhoc_comm::msgWrapper::POSE_STAMPED_ID) 
    { 
      geometry_msgs::PoseStamped msg;
      // unpack received message into the above msg
      msg_wrapper_->unpackPoseStamped(buffer, acl::adhoc_comm::UDPSocket::PACKET_LENGTH, msg);
      // publish the message on its respective topic
      pub_loc_in_.publish(msg);

      ROS_INFO_STREAM("RECEPTOR:\tReceived PoseStamped packet.\n");
    }

    else  //the packet is false
      ROS_INFO_STREAM("RECEPTOR:\tReceived false packet.\n");
  }
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void ReceptorROS::init() {
  std::string hostIP;
  int commPort;
  nhp_.param<std::string>("hostIP", hostIP, "192.168.0.184");
  nhp_.param<int>("commPort", commPort, 35757);
  nhp_.param<int>("deviceID", deviceID_, 0);

  // initialize adhoc communication variables
  udp_socket_.reset( new acl::adhoc_comm::UDPSocket(hostIP, commPort) );
  olsrd_.reset( new acl::adhoc_comm::OLSRD_Wrapper(hostIP) );
  msg_wrapper_.reset( new acl::adhoc_comm::msgWrapper(deviceID_) );

}

// ----------------------------------------------------------------------------

void ReceptorROS::sendPoseStamped(const geometry_msgs::PoseStamped& msg) {
  ROS_INFO_STREAM("RECEPTOR:\tSending a packet\n");
  

  // update the olsrd neighbors
  olsrd_->updOLSRDneighbors();
  // display oslrd neighbors
  olsrd_->printNeighbors();

  // create a buffer for the packet
  uint8_t buffer[acl::adhoc_comm::UDPSocket::PACKET_LENGTH]; 
  
  // insert data into the buffer
  size_t buflen = msg_wrapper_->packPoseStamped(buffer, msg);

  // send out data to each neighbor
  for (auto neighbor = olsrd_->neighbors_.begin(); neighbor != olsrd_->neighbors_.end(); ++neighbor)
    udp_socket_->sendCommUDP(*neighbor, (char *) buffer, buflen); 


  ROS_INFO_STREAM("RECEPTOR:\tPacket Sent\n");
}

} // ns utils
} // ns acl
