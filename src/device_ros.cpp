/**
 * @file device_ros.cpp
 * @brief ROS wrapper for the device node
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 Sep 2020
 */

#include "device/device_ros.h"
#include <chrono>
#include <string.h>
#include <stdio.h>

namespace acl {
namespace device {

DeviceROS::DeviceROS(const ros::NodeHandle nh, const ros::NodeHandle nhp)
: nh_(nh), nhp_(nhp)
{
  // subcribe to incoming messages from the receptor node
  sub_loc_in_ = nh_.subscribe("receptor/loc_in", 1, &DeviceROS::processLocData, this);
  // publish position messages for the receptor node to receive
  pub_loc_out_ = nhp_.advertise<geometry_msgs::PoseStamped>("loc_out", 1);

  // send messages at this rate
  timer_loc_udp_ = nh_.createTimer(ros::Duration(3.0), &DeviceROS::publishLocation, this);
  
  ROS_INFO_STREAM("DEVICE:\tInit complete.\n");
}

// ----------------------------------------------------------------------------
// Callbacks
// ----------------------------------------------------------------------------

void DeviceROS::processLocData(const geometry_msgs::PoseStamped& msg) {
  ROS_INFO_STREAM("DEVICE:\tMessage received\n");
  std::cout << msg << "\n\n";
}

// ----------------------------------------------------------------------------

void DeviceROS::publishLocation(const ros::TimerEvent& event) {
  // publihs a message to be UDPed
  geometry_msgs::PoseStamped msg;
  msg.header.stamp = ros::Time::now();
  
  msg.pose.position.x = 1.0;
  msg.pose.position.y = 2.0;
  msg.pose.position.z = 3.0;
  
  msg.pose.orientation.x = 4.0;
  msg.pose.orientation.y = 5.0;
  msg.pose.orientation.z = 6.0;
  msg.pose.orientation.w = 7.0;

  pub_loc_out_.publish(msg);

  ROS_INFO_STREAM("DEVICE:\tSending a message\n");
}

} // ns pendulum
} // ns acl
