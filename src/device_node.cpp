/**
 * @file device_node.cpp
 * @brief An example device node
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 Sep 2020
 */

#include <ros/ros.h>

#include "device/device_ros.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "device");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhptopics("~");
  acl::device::DeviceROS node(nhtopics, nhptopics);
  ros::spin();
  return 0;
}
