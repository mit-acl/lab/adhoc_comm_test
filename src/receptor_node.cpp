/**
 * @file receptor_node.cpp
 * @brief The receptor node
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 Sep 2020
 */

#include <ros/ros.h>
#include "device/receptor_ros.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "receptor");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  acl::receptor::ReceptorROS node(nhtopics, nhparams);

  // allow multi-threading;
  // necessary because listening to incoming UDP packets blocks the thread
  // one thread for receiving packets, one thread for sending packets
  ros::AsyncSpinner spinner(2);
  spinner.start();
  while (1)
    node.listenToUDP();
  return 0;
}
