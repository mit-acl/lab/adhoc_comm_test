Receptor Node Demo: Ad-hoc Communication Example.
===================

## Overview

This ROS package is an example / template for [decentralized ROS-based ad-hoc multirobot communication](https://gitlab.com/mit-acl/lab/adhoc_comm). 

### Description and Features:
* This package is the implementation template for the adhoc_comm utility package, which contains wrappers for socket communication, routing, and message serialization.
* Each robot runs its own ros_master and roscore, removing the centralized ros-master single point of failure.
* Robots communicate and discover each other via an ad-hoc network. Robots create their own routing tables using the [OLSR routing protocol](http://www.olsr.org/mediawiki/index.php/Olsr_Daemon). This removes the central router single point of failure.
* On the ROS level, robots communicate between each other via receptor nodes. Each robot has a receptor node that communicates with receptor nodes on other robots via UDP sockets. The receptor node abstracts all other nodes from the communication layer: all other ROS nodes on the robot keep publishing as before, unaware that the information is transmitted to their recepient via the receptor nodes.

## Receptor node description

The receptor node uses the adhoc_comm wrappers to communicate with the receptor nodes on other devices.
The receptor node would be subscribed to each topic that is to be shared with other robots, and will in turn publish all topics that it is supposed to receive from other robots (in the framework of this package, these topics are ```loc_out``` for messages to be UDP-ed to other robots, and ```loc_in``` for the messages to be received from other robots). 
The receptor runs two process threads: one for receiving new UDP messages from other robots, and another to send UDP messages publisher by its own robot's devices. 
The receiving thread is blocking: it continiously spins and waits for an incoming UDP packet; once a packet is received, the receptor checks the packet type, serializes the packet into an appropriate message, and publishes it on an appropiriate ros topic.
The sending thread deals with callbacks to the topics that the receptor node is subscribed to. Each ros message that the receptor node receives gets serialized into a binary and sent to all of its network neighbors, determined from the olsrd daemon.

ROS data to and from the receptor node is spublished / received by the device node.


## Usage and Installation

1. First follow the ```adhoc_comm``` installation procedure. Make sure to install ```geometry_msgs``` package as well.

2. Download this package from git, unzip, add to the ```src``` directory of your catkin workspace, ```catkin_make``` it.

3. Adjust the param file to match your specific networking case; in particular, make sure that for each robot, the hostIP address matches robot's IP address on the network used for ad-hoc and olsrd communication. If IP address doesn't belong to the host machine, the code will thow socket errors. If the IP address is not the one that olsrd uses in its routing, OLSRd will return no neighbors.

4. Run the olsrd daemon in the background on each machine (see ```adhoc_comm``` usage and installation section).

5. ```roscore``` on each machine. On each machine, launch this demo with ```roslaunch adhoc_comm_test device.launch```.


